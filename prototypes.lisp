(defpackage :prototypes
  (:export #:prototype-object
           #:prototype-delegates
           #:prototype-add-delegate
           #:prototype-remove-delegate
           #:make-prototype)
  (:use :cl :closer-mop))

(in-package :prototypes)

;;;;
;;;; PROTOTYPE-OBJECT
;;;;

(defclass prototype-object ()
  ((%delegates :initarg :delegates
               :reader prototype-delegates
               :writer %set-prototype-delegates
               :initform nil))
  (:documentation "The root of the prototype hierarchy. Instantiate this
  class to create a new prototype, possibly initializing it with
  a :DELEGATES argument to provide a list of other prototype instances
  that slots will be inherited from."))

(defgeneric prototype-add-delegate (object delegate)
  ;; TODO: test case
  ;; Maybe indicate whether delegate was already there?
  (:documentation "Adds a DELEGATE to the end of OBJECT's delegates, if
  it is not already there. Returns no values.")
  (:method ((object prototype-object) (delegate prototype-object))
    (if (prototype-delegates object)
        (loop for tail on (prototype-delegates object)
           until (eql delegate (car tail))
           finally (setf (cdr tail) (list delegate)))
        (%set-prototype-delegates (list delegate) object))
    (values)))

(defgeneric prototype-remove-delegate (object delegate)
  ;; TODO: test case
  ;; Maybe indicate whether delegate was actually found?
  (:documentation "Removes DELEGATE from OBJECT's delegates, if it is
  there. Returns no values.")
  (:method ((object prototype-object) (delegate prototype-object))
    (%set-prototype-delegates (delete delegate (prototype-delegates object))
                              object)
    (values)))

;;;;
;;;; Utility for memoization of searches
;;;;

(defun symbolicate (x) 
  (make-symbol (write-to-string x :escape nil)))

(defun memoize-method-result (generic-function specializers result)
  (restart-case
      (add-method generic-function
                  (make-instance 'standard-method
                                 :lambda-list (mapcar #'symbolicate
                                                      specializers)
                                 :specializers specializers
                                 :function (constantly result)))
    (disable-memoization ()
      :report "Disable memoization and continue."
      (setf (symbol-function 'memoize-method-result) (constantly nil)))))

;;;;
;;;; Prototype backend class search and generation
;;;;

(defgeneric prototype-find-subclass (prototype slot-name))

(defmethod prototype-find-subclass ((object prototype-object) (slot-name symbol))
  (let ((subclass (find slot-name
                        (class-direct-subclasses (class-of object))
                        :key (lambda (subclass) 
                               (slot-definition-name (first (class-direct-slots subclass)))))))
    (when subclass
      (memoize-method-result #'prototype-find-subclass
                             (list (class-of object)
                                   (intern-eql-specializer slot-name))
                             subclass))
    subclass))

(defun prototype-subclass (object slot-name)
  (make-instance 'standard-class
                 :direct-superclasses (list (class-of object))
                 :direct-slots (list (list :name slot-name :initargs (list slot-name)))))

(defun ensure-subclass (class slot-name)
  (or (prototype-find-subclass class slot-name)
      (prototype-subclass class slot-name)))

;;;;
;;;; Additional functionality needed for prototype object manipulation
;;;; beyond what CLOS gives us for free
;;;;

;;; TODO: Delegate down a linearized precedence list. Maybe offer both
;;; CLOS and C3 linearization algorithms.
(macrolet ((reader-delegation (operation)
             `(defmethod slot-missing (class (object prototype-object)
                                       slot-name (operation (eql ',operation))
                                       &optional new-value)
                (declare (ignore new-value))
                (dolist (delegate (prototype-delegates object)
                         ;; if no slot is found in all the delegates, we
                         ;; call the default method to signal a
                         ;; slot-missing error
                         (call-next-method))
                  (handler-bind ((unbound-slot #'error)
                                 ;; there is no specific class for
                                 ;; slot-missing errors. the spec just
                                 ;; says signals an error of type error.
                                 ;; ugh.
                                 (error 
                                  ;; can't find the slot here, so we
                                  ;; continue to the next delegate
                                  #'identity))
                    ;; if this finds the slot, we return it
                    (return (,operation delegate slot-name)))))))
  (reader-delegation slot-value)
  (reader-delegation slot-boundp))

(macrolet ((writer-subclassing (operation &rest initargs)
             `(defmethod slot-missing (class (object prototype-object)
                                       slot-name (operation (eql ',operation))
                                       &optional new-value)
                (let ((new-class (ensure-subclass object slot-name)))
                  (change-class object new-class ,@initargs)))))
  (writer-subclassing setf slot-name new-value)
  (writer-subclassing slot-makunbound))

;;;;
;;;; Shortcut for single-inheritance
;;;;

(defmethod make-instance ((prototype prototype-object) &rest initargs
                          &key &allow-other-keys)
  "Create a PROTOTYPE-OBJECT that delegates to the given PROTOTYPE."
  (let ((object (make-instance 'prototype-object :delegates (list prototype))))
    (loop for (slot-name value) on initargs by #'cddr
       do (setf (slot-value object slot-name) value))
    object))

;;;;
;;;; Subclassing of CLOS classes as prototype objects
;;;;

(defgeneric find-std-class-prototype (class))

(defmethod find-std-class-prototype ((class standard-class))
  (let ((subclass (find-if (lambda (subclass) (subtypep subclass class))
                           (class-direct-subclasses (find-class 'prototype-object))
                           :key (lambda (subclass) 
                                  (first (class-direct-superclasses subclass))))))
    (when subclass
      (memoize-method-result #'find-std-class-prototype
                             (list (intern-eql-specializer class))
                             subclass))
    subclass))

(defun make-std-class-prototype (class)
  (make-instance 'standard-class
                 :direct-superclasses (list class 
                                            (find-class 'prototype-object))))

(defun ensure-std-class-prototype (class)
  (or (find-std-class-prototype class)
      (make-std-class-prototype class)))

(defgeneric make-prototype (class &rest initargs
                                  &key delegates &allow-other-keys)
  (:documentation "Create a prototype instance that is an instance of
  CLASS, initializing it with the given INITARGS, which may
  include :DELEGATES to specify the instance's delegates."))

(defmethod make-prototype ((class-name symbol) &rest initargs)
  (apply #'make-prototype (find-class class-name) initargs))

(defmethod make-prototype ((class standard-class) &rest initargs)
  (apply #'make-instance (ensure-std-class-prototype class)
         initargs))

;;;;
;;;; TESTS
;;;;

(defparameter *1* (make-instance 'prototype-object))
(setf (slot-value *1* 'x) 1)

(defparameter *2* (make-instance *1*))

(assert (eql (slot-value *2* 'x) 1))

(defparameter *3* (make-instance *2*))

(assert (eql (slot-value *3* 'x) 1))

(defparameter *3.1* (make-instance *2*))

(assert (eql (slot-value *3.1* 'x) 1))

(defparameter *3.3.1* (make-instance 'prototype-object :delegates (list *3.1* *3*)))

(setf (slot-value *2* 'x) 2)

(assert (eql (slot-value *2* 'x) 2))
(assert (eql (slot-value *3* 'x) 2))
(assert (eql (slot-value *3.1* 'x) 2))
(assert (eql (slot-value *3.3.1* 'x) 2))

(setf (slot-value *3* 'x) 3)

(assert (eql (slot-value *2* 'x) 2))
(assert (eql (slot-value *3* 'x) 3))
(assert (eql (slot-value *3.1* 'x) 2))
(assert (eql (slot-value *3.3.1* 'x) 2))

(slot-makunbound *3.1* 'x)

(assert (eql (slot-value *2* 'x) 2))
(assert (eql (slot-value *3* 'x) 3))
(assert (not (slot-boundp *3.1* 'x)))
(assert (not (slot-boundp *3.3.1* 'x)))
(handler-case
    (progn
      (slot-value *3.3.1* 'x)
      (assert nil))
  (unbound-slot (error)))

(defclass test ()
  ((x :allocation :class)))

;; need to always reset the slot value because we change it below
(finalize-inheritance (find-class 'test))
(setf (slot-value (class-prototype (find-class 'test)) 'x) :test)

(defparameter *t* (make-prototype 'test))

(assert (eql (slot-value *t* 'x) :test))

(defparameter *3.t* (make-instance 'prototype-object
                                   :delegates (list *3* (make-prototype 'test))))

(assert (eql (slot-value *3.t* 'x) 3))

(defparameter *t.3* (make-prototype 'test :delegates (list *3*)))

(assert (eql (slot-value *t.3* 'x) :test))

(setf (slot-value *t* 'x) :t)

(assert (eql (slot-value *t* 'x) :t))
(assert (eql (slot-value *3.t* 'x) 3))
(assert (eql (slot-value *t.3* 'x) :t)) ; The slot is class-allocated, remember!
